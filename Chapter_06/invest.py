def invest(initial_amount, rate, number_of_years):
    """Display year on year growth of an initial investment"""
    for year in range(1, number_of_years + 1):
        initial_amount = initial_amount * (1 + rate)
        print(f"year {year}: ${initial_amount:,.2f}")


initial_amount = float(input("Enter a initial amount: "))
rate = float(input("Enter an anual rate of return: "))
number_of_years = int(input("Enter a number of years: "))

invest(initial_amount, rate, number_of_years)