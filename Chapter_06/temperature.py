def convert_far_to_cel(F):
    C = (F-32) * 5/9
    return C


def convert_cel_to_far(C):
    F = C * 9/5 +32
    return F

F = float(input('Enter a temperature in degrees F: '))
C = convert_far_to_cel(float(F))
print(f"{F} degrees F = {C:.2f} degrees C")


C = float(input('Enter a temperature in degrees C: '))
F = convert_cel_to_far(float(C))
print(f"{C} degrees C = {F:.2f} degrees F")



