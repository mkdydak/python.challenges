import random


nouns = ['fosil', 'horse', 'aardvark', 'judge', 'chef', 'mango', 'extrovert', 'gorilla']

verbs = ['kicks', 'jingles', 'bounces', 'slurps', 'meows', 'explodes', 'curdles']

adjectives = ['furry', 'balding', 'incredulous', 'fragrant', 'exuberant', 'glistening']

prepositions = ['against', 'after', 'into', 'beneath', 'upon', 'for', 'in', 'like', 'over', 'within']

adverbs = ['curiously', 'extravagantly', 'tantalizingly', 'furiously', 'sensuously']

def make_poem():

    # 3 nouns
    noun1 = random.choice(nouns)
    noun2 = random.choice(nouns)
    noun3 = random.choice(nouns)
    while noun1 == noun2:
        noun2 = random.choice(nouns)
    while noun1 == noun3 or noun2 == noun3:
        noun3 = random.choice(nouns)

    # 3 verbs
    verb1 = random.choice(verbs)
    verb2 = random.choice(verbs)
    verb3 = random.choice(verbs)
    while verb1 == verb2:
        verb2 = random.choice(verbs)
    while verb1 == verb3 or verb2 == verb3:
        verb3 = random.choice(verbs)

    # 2 adjectives  verb1 = random.choice(verbs)
    adj1 = random.choice(adjectives)
    adj2 = random.choice(adjectives)
    adj3 = random.choice(adjectives)
    while adj1 == adj2:
        adj2 = random.choice(adjectives)
    while adj1 == adj3 or adj2 == adj3:
        adj3 = random.choice(adjectives)

    # 2 prepositions
    prep1 = random.choice(prepositions)
    prep2 = random.choice(prepositions)
    while prep1 == prep2:
        prep2 = random.choice(prepositions)

    # 1 adverb
    adv1 = random.choice(adverbs)

    if "aeiou".find(adj1[0]) != -1:  
        article = "An"
    else:
        article = "A"

   
    poem = (
        f"{article} {adj1} {noun1}\n\n"
        f"{article} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}\n"
        f"{adv1}, the {noun1} {verb2}\n"
        f"the {noun2} {verb3} {prep2} a {adj3} {noun3}"
    )

    return poem


poem = make_poem()
print(poem)