cats = [False] * 100  

for round in range(1, 101): 
    for cat in range(100): 
        if (cat + 1) % round == 0:  
            cats[cat] = not cats[cat]  

for i in range(len(cats)):
    if cats[i]: 
        print(f"Cat #{i + 1} has a hat.")