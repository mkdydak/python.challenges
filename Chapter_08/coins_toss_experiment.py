import random

def coins_flip():
    return random.choice(["head", "tail"])

flips = 0
num_trials = 10_000

for trial in range(num_trials):
    flips = flips + 1
    current_result = coins_flip()

    while current_result == "head":
        flips = flips + 1
        current_result = coins_flip()

    flips = flips + 1

avg_flips_per_trial = flips / num_trials
print(f"The average number of flips per trial is {avg_flips_per_trial}.")