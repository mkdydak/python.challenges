from random import random

first_candidate = 0
second_candidate = 0

trials = 10_000 

for x in range(0, trials):
    votes_first_candidate = 0
    votes_second_candidate = 0 

    if random() < 0.87:
        votes_first_candidate += 1
    else:
        votes_second_candidate += 1

    if random() < 0.65:
        votes_first_candidate += 1
    else:
        votes_second_candidate += 1

    if random() < 0.17:
        votes_first_candidate += 1
    else:
        votes_second_candidate += 1

    if votes_first_candidate > votes_second_candidate:
        first_candidate += 1
    else:
        second_candidate += 1

print(f"Probability A wins: {first_candidate / trials}")
print(f"Probability B wins: {second_candidate / trials}")